import javax.activation.DataHandler
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMultipart
import groovy.text.GStringTemplateEngine
import org.apache.log4j.Logger
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.mail.util.ByteArrayDataSource
import com.atlassian.mail.Email
import com.atlassian.jira.web.bean.PagerFilter
import org.springframework.util.FileCopyUtils


integrationUtils = new IntegrationUtilsTool()

/**
 * Utility library for the JIRA PDF View Plugin and the Better Excel Plugin for JIRA,
 * to integrate PDF document and Excel spreadsheet exporting capabilities with your JIRA workflows, services and plugins.
 *
 * @see http://www.midori-global.com/products/jira-pdf-view-plugin/documentation/api
 * @see http://www.midori-global.com/products/jira-better-excel-plugin/documentation/api
 */
public class IntegrationUtilsTool {
	def log = Logger.getLogger("IntegrationUtils.groovy")

	/**
	 * Renders a PDF document using the PDF API and returns the "result" object containing its filename and bytes.
	 */
	def getPdf(def templateName, def title, def issues) {
		// render PDF
		def pdfApiClass = this.class.forName('com.midori.jira.plugin.pdfview.api.PdfApi', true, ComponentManager.instance.pluginAccessor.classLoader)
		log.debug("PDF API class found: <${pdfApiClass}>")
		def pdfApi = ComponentManager.getOSGiComponentInstanceOfType(pdfApiClass)
		log.debug("PDF API instance found: ${pdfApi}")
		def pdfResult = pdfApi.getPdf(templateName, title.toString(), issues, [:])
		log.debug("PDF content rendered: ${pdfResult.bytes.length} bytes")

		return pdfResult
	}

	/**
	 * Renders an Excel spreadsheet using the XLS API and returns the "result" object containing its filename and bytes.
	 */
	def getXls(def templateName, def title, def issues) {
		// render XLS
		def xlsApiClass = this.class.forName('com.midori.jira.plugin.betterexcel.api.XlsApi', true, ComponentManager.instance.pluginAccessor.classLoader)
		log.debug("XLS API class found: <${xlsApiClass}>")
		def xlsApi = ComponentManager.getOSGiComponentInstanceOfType(xlsApiClass)
		log.debug("XLS API instance found: ${xlsApi}")
		def xlsResult = xlsApi.getXls(templateName, title.toString(), issues, [:])
		log.debug("XLS content rendered: ${xlsResult.bytes.length} bytes")

		return xlsResult
	}

	/**
	 * Sends an email to the "to" email address, using JIRA's built-in email queue and attaching the PDF or Excel file passed in "result".
	 */
	def sendFileInEmail(def to, def subject, def body, def result) {
		// get the SMTP server
		def mailServerManager = ComponentManager.instance.mailServerManager
		def smtpMailServer = mailServerManager.defaultSMTPMailServer

		if(smtpMailServer != null) {
			// send mail
			def dataSource = new ByteArrayDataSource(new ByteArrayInputStream(result.bytes), result.fileName.toLowerCase().endsWith("pdf") ? "application/pdf" : "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")

			def attachmentPart = new MimeBodyPart(dataHandler:new DataHandler(dataSource), fileName:result.fileName)

			def multipart = new MimeMultipart("mixed")
			multipart.addBodyPart(attachmentPart)

			def email = new Email(to)
			email.subject = subject
			email.body = body
			email.multipart = multipart

			smtpMailServer.send(email)
			log.debug("Email \"${subject}\" sent to <${to}>")
		} else {
			log.error("No SMTP server is defined in JIRA, unable to send email")
		}
	}

	/**
	 * Saves the PDF or Excel file passed in "result" to "path" in the file system.
	 */
	def copyFileToPath(def result, def path) {
		log.debug("Copying file to <${path}>")
		FileCopyUtils.copy(result.bytes, new File(path))
	}

	def getSavedFilter(def user, def savedFilterId) {
		return ComponentManager.instance.searchRequestManager.getSearchRequestById(user, savedFilterId)
	}

	/**
	 * Executes a saved filter  and returns the list of issues resulted.
	 */
	def runSavedFilter(def user, def savedFilterId) {
		def searchRequest = getSavedFilter(user, savedFilterId)
		if(searchRequest == null) {
			log.warn("Filter #${savedFilterId} not found")
			return null
		}

		def searchResults = ComponentManager.instance.searchProvider.search(searchRequest.query, user, PagerFilter.getUnlimitedFilter())
		if(searchResults == null) {
			log.warn("No results found when running ${searchRequest.name}")
			return null
		}

		log.debug("${searchResults.total} issues found when running \"${searchRequest.name}\" as <${user.name}>")

		return searchResults.issues
	}

	/**
	 * Adds a comment to the passed issue on behalf of the passed user.
	 */
	def addIssueComment(def issue, def user, def comment) {
		def commentManager = ComponentManager.instance.commentManager
		commentManager.create(issue, user, comment, true)
	}

	/**
	 * Returns the value of a transition property. If the property value contains GString placeholders like "${issue.key}",
	 * those will be replaced with their actual values.
	 */
	def getWorkflowTransitionProperty(def transitionProperties, def key, def defaultValue, def binding) {
		if(!transitionProperties.containsKey(key)) {
			return defaultValue
		}

		// render transition property value
		def templateValue = transitionProperties.get(key)
		def templateEngine = new GStringTemplateEngine()
		def value = templateEngine.createTemplate(templateValue).make(binding).toString()

		return value
	}
}