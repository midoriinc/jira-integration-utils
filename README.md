### What is this repository for?

This repository contains the source code of the _IntegrationUtils_ library.
This library is used by the automation scripts which export Excel spreadsheets and PDF documents from Jira with the help of the Midori exporter apps.

See these tutorials for details:

1. Better Excel Exporter for Jira: https://www.midori-global.com/products/better-excel-exporter-for-jira/data-center/documentation/api
2. Better PDF Exporter for Jira: https://www.midori-global.com/products/better-pdf-exporter-for-jira/data-center/documentation/api

